# WorldCupScoreBoard

## Description

WorldCupScoreBoard is the implementation of the Live Football World Cup Score Board
as library from "Live Odds Services" - com.los.world-cup-score-board v 1.0

## Start locally

1. After pulling from repository you can run test using _gradle clean test_.
2. Also, you can publish this library in your localMaven repository
and use this "ScoreBoardService" in your own application,
for this use _gradle publishToMavenLocal_

## Input data requirements

1. Home and away team can't be switched in MatchDTO:
    _match-1: {
        homeTeam: "USA",
        awayTeam: "Brazil"
    }_
and
   _match-2: {
        homeTeam: "Brazil",
        awayTeam: "USA"
   }_.
There are different matches, in case if we have on board match-1 and when to try start, finish
or update match-2 will throw Exception.
2. Name of teams in MatchDTO can use with ignore case.
3. ScoreDTO fields should contain only digits.

### Teams
1. Teams' name in the lib using as String (not ENUM) to simplicity.

2. Matches are adding to the board without Date and Time of start match, for simplicity,
order of teams at the board set according adding matches to the board.

## Return summary

Summary of matches from scoreboard is returned as a list of strings, for simplicity.
(ex. ["Uruguay 6 - Italy 6", "Spain 10 - Brazil 2"])