package com.los.worldcupscoreboard.data;

import com.los.worldcupscoreboard.data.dto.MatchDTO;
import com.los.worldcupscoreboard.data.dto.ScoreDTO;
import com.los.worldcupscoreboard.entity.Match;

import java.util.List;

public final class TestData {

    //Check work with one newMatch
    public static MatchDTO newMatchDto1() {
        return new MatchDTO("Poland", "UAE");
    }

    public static MatchDTO notCorrectNewMatchDto1() {
        return new MatchDTO("Poland", "Mexico");
    }

    public static MatchDTO notCorrectNewMatchDto2() {
        return new MatchDTO("UAE", "Poland");
    }

    public static MatchDTO notCorrectNewMatchDto3() {
        return new MatchDTO("Poland", "Poland");
    }

    public static Match newMatch1() {
        return Match.builder()
                .homeTeam("Poland")
                .awayTeam("UAE")
                .build();
    }

    public static List<Match> scoreBoardOneNewMatch() {
        return List.of(
                newMatch1()
        );
    }

    public static ScoreDTO scoreDtoChangedNewMatch1() {
        return new ScoreDTO(2, 0);
    }

    public static ScoreDTO notCorrectScoreDtoChangedNewMatch1() {
        return new ScoreDTO(0, 0);
    }

    public static Match changedNewMatch1() {
        return Match.builder()
                .homeTeam("Poland")
                .awayTeam("UAE")
                .homeScore(2)
                .awayScore(0)
                .build();
    }

    //Test data for 5 Matches

    public static Match match1() {
        return Match.builder()
                .homeTeam("Mexico")
                .awayTeam("Canada")
                .homeScore(0)
                .awayScore(5)
                .build();
    }

    public static MatchDTO matchDtoForMatch1() {
        return new MatchDTO("Mexico", "Canada");
    }

    public static ScoreDTO scoreDtoForMatch1() {
        return new ScoreDTO(8, 5);
    }

    public static ScoreDTO notCorrectScoreDtoForMatch1() {
        return new ScoreDTO(1, 2);
    }

    public static Match updatedMatch1() {
        return Match.builder()
                .homeTeam("Mexico")
                .awayTeam("Canada")
                .homeScore(8)
                .awayScore(5)
                .build();
    }

    public static Match match2() {
        return Match.builder()
                .homeTeam("Spain")
                .awayTeam("Brazil")
                .homeScore(10)
                .awayScore(2)
                .build();
    }

    public static Match match3() {
        return Match.builder()
                .homeTeam("Germany")
                .awayTeam("France")
                .homeScore(2)
                .awayScore(2)
                .build();
    }

    public static Match match4() {
        return Match.builder()
                .homeTeam("Uruguay")
                .awayTeam("Italy")
                .homeScore(6)
                .awayScore(6)
                .build();
    }

    public static MatchDTO matchDtoForMatch4() {
        return new MatchDTO("Uruguay", "Italy");
    }

    public static ScoreDTO scoreDtoForMatch4() {
        return new ScoreDTO(7, 7);
    }

    public static ScoreDTO notCorrectScoreDtoForMatch4() {
        return new ScoreDTO(7, 5);
    }

    public static Match updatedMatch4() {
        return Match.builder()
                .homeTeam("Uruguay")
                .awayTeam("Italy")
                .homeScore(7)
                .awayScore(7)
                .build();
    }

    public static Match match5() {
        return Match.builder()
                .homeTeam("Argentina")
                .awayTeam("Australia")
                .homeScore(3)
                .awayScore(1)
                .build();
    }

    public static List<Match> scoreBoardFiveMatches() {
        return List.of(
                match1(),
                match2(),
                match3(),
                match4(),
                match5()
        );
    }

    public static List<String> summaryForFiveMatches() {
        return List.of(
                "Uruguay 6 - Italy 6",
                "Spain 10 - Brazil 2",
                "Mexico 0 - Canada 5",
                "Argentina 3 - Australia 1",
                "Germany 2 - France 2"
        );
    }

    public static List<Match> updatedScoreBoardFiveMatches() {
        return List.of(
                updatedMatch1(),
                match2(),
                match3(),
                updatedMatch4(),
                match5()
        );
    }

    public static List<String> summaryForFiveUpdatedMatches() {
        return List.of(
                "Uruguay 7 - Italy 7",
                "Mexico 8 - Canada 5",
                "Spain 10 - Brazil 2",
                "Argentina 3 - Australia 1",
                "Germany 2 - France 2"
        );
    }
}
