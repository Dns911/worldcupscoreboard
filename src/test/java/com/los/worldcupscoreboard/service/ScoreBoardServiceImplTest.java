package com.los.worldcupscoreboard.service;

import com.los.worldcupscoreboard.data.TestData;
import com.los.worldcupscoreboard.entity.Match;
import com.los.worldcupscoreboard.exception.InputDataException;
import com.los.worldcupscoreboard.exception.MatchAlreadyExistException;
import com.los.worldcupscoreboard.exception.MatchNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
class ScoreBoardServiceImplTest {

    @Autowired
    ScoreBoardService scoreBoardService;

    void addToScoreBoardNewMatch() {
        scoreBoardService.getScoreBoard().addAll(TestData.scoreBoardOneNewMatch());
    }

    void addToScoreBoardAllMatches() {
        scoreBoardService.getScoreBoard().addAll(TestData.scoreBoardFiveMatches());
    }

    void addToScoreBoardAllUpdatedMatches() {
        scoreBoardService.getScoreBoard().addAll(TestData.updatedScoreBoardFiveMatches());
    }

    @BeforeEach
    void beforeEach() {
        scoreBoardService.getScoreBoard().clear();
    }

    @Test
    void testStartMatch1_Success() throws MatchAlreadyExistException, InputDataException {
        //when
        scoreBoardService.startMatch(TestData.newMatchDto1());
        List<Match> actual = scoreBoardService.getScoreBoard();
        List<Match> expected = TestData.scoreBoardOneNewMatch();

        //then
        assertEquals(expected, actual);
    }

    @Test
    void testStartMatch2_Success() throws MatchAlreadyExistException, InputDataException {
        //given
        addToScoreBoardAllMatches();
        //when
        scoreBoardService.startMatch(TestData.newMatchDto1());
        int actual = scoreBoardService.getScoreBoard().size();
        int expected = 6;

        //then
        assertEquals(expected, actual);
    }

    @Test
    void testStartMatchExistMatch_ThrowMatchExistException() {
        //given
        addToScoreBoardNewMatch();
        //then
        assertThrows(MatchAlreadyExistException.class, () -> scoreBoardService.startMatch(TestData.newMatchDto1()));
    }

    @Test
    void testStartMatchExistTeam1_throwMatchExistException() {
        //given
        addToScoreBoardNewMatch();
        //then
        assertThrows(MatchAlreadyExistException.class, () -> scoreBoardService.startMatch(TestData.notCorrectNewMatchDto1()));
    }

    @Test
    void testStartMatchExistTeam2_throwMatchExistException() {
        //given
        addToScoreBoardNewMatch();
        //then
        assertThrows(MatchAlreadyExistException.class, () -> scoreBoardService.startMatch(TestData.notCorrectNewMatchDto2()));
    }

    @Test
    void testStartMatchExistTeam3_throwMatchExistException() {
        //given
        addToScoreBoardAllMatches();
        //then
        assertThrows(MatchAlreadyExistException.class, () -> scoreBoardService.startMatch(TestData.matchDtoForMatch1()));
    }

    @Test
    void testStartMatch_throwInputDataException() {
        //given
        addToScoreBoardNewMatch();
        //then
        assertThrows(InputDataException.class, () -> scoreBoardService.startMatch(TestData.notCorrectNewMatchDto3()));
    }

    @Test
    void testUpdateMatch2_Success() throws MatchNotFoundException, InputDataException {
        //given
        addToScoreBoardAllMatches();
        //when
        Match actual = scoreBoardService.updateMatch(TestData.matchDtoForMatch1(), TestData.scoreDtoForMatch1());
        Match expected = TestData.updatedMatch1();
        //then
        assertEquals(expected, actual);
    }

    @Test
    void testUpdateMatch3_Success() throws MatchNotFoundException, InputDataException {
        //given
        addToScoreBoardAllMatches();
        //when
        Match actual = scoreBoardService.updateMatch(TestData.matchDtoForMatch4(), TestData.scoreDtoForMatch4());
        Match expected = TestData.updatedMatch4();
        //then
        assertEquals(expected, actual);
    }

    @Test
    void testUpdateMatch4_Success() throws MatchNotFoundException, InputDataException {
        //given
        addToScoreBoardAllMatches();
        //when
        scoreBoardService.updateMatch(TestData.matchDtoForMatch1(), TestData.scoreDtoForMatch1());
        scoreBoardService.updateMatch(TestData.matchDtoForMatch4(), TestData.scoreDtoForMatch4());
        List<Match> actual = scoreBoardService.getScoreBoard();
        List<Match> expected = TestData.updatedScoreBoardFiveMatches();
        //then
        assertEquals(expected, actual);
    }

    @Test
    void testUpdateMatchNotCorrectScore2_ThrowInputDataException() {
        //given
        addToScoreBoardAllMatches();
        //then
        assertThrows(InputDataException.class, () ->
                scoreBoardService.updateMatch(TestData.matchDtoForMatch1(), TestData.notCorrectScoreDtoForMatch1()));
    }

    @Test
    void testUpdateMatchNotCorrectScore3_ThrowInputDataException() {
        //given
        addToScoreBoardAllMatches();
        //then
        assertThrows(InputDataException.class, () ->
                scoreBoardService.updateMatch(TestData.matchDtoForMatch4(), TestData.notCorrectScoreDtoForMatch4()));
    }

    @Test
    void testUpdateMatch1_Success() throws InputDataException, MatchNotFoundException {
        //given
        addToScoreBoardNewMatch();
        //when
        Match actual = scoreBoardService.updateMatch(TestData.newMatchDto1(), TestData.scoreDtoChangedNewMatch1());
        Match expected = TestData.changedNewMatch1();
        //then
        assertEquals(expected, actual);
    }

    @Test
    void testUpdateMatchNotCorrectScore1_ThrowInputDataException() throws InputDataException, MatchNotFoundException {
        //given
        addToScoreBoardNewMatch();
        scoreBoardService.updateMatch(TestData.newMatchDto1(), TestData.scoreDtoChangedNewMatch1());
        //then
        assertThrows(InputDataException.class,
                () -> scoreBoardService.updateMatch(TestData.newMatchDto1(), TestData.notCorrectScoreDtoChangedNewMatch1()));
    }

    @Test
    void testUpdateMatchNotCorrectTeam1_ThrowInputDataException() {
        //given
        addToScoreBoardNewMatch();
        //then
        assertThrows(InputDataException.class,
                () -> scoreBoardService.updateMatch(TestData.notCorrectNewMatchDto3(), TestData.scoreDtoChangedNewMatch1()));
    }

    @Test
    void testFinishMatch1_Success() throws InputDataException, MatchNotFoundException {
        //given
        addToScoreBoardNewMatch();
        //when
        scoreBoardService.finishMatch(TestData.newMatchDto1());

        int actual = scoreBoardService.getScoreBoard().size();
        int expected = 0;
        //then
        assertEquals(expected, actual);
    }

    @Test
    void testFinishMatch2_Success() throws InputDataException, MatchNotFoundException {
        //given
        addToScoreBoardAllMatches();
        //when
        scoreBoardService.finishMatch(TestData.matchDtoForMatch4());

        int actual = scoreBoardService.getScoreBoard().size();
        int expected = 4;
        //then
        assertEquals(expected, actual);
    }

    @Test
    void testFinishMatch1_ThrowMatchNotFoundException() {
        //given
        addToScoreBoardNewMatch();

        assertThrows(MatchNotFoundException.class, () ->
                scoreBoardService.finishMatch(TestData.notCorrectNewMatchDto1()));
    }

    @Test
    void testFinishMatch1_ThrowInputDataException() {
        //given
        addToScoreBoardNewMatch();

        assertThrows(InputDataException.class, () ->
                scoreBoardService.finishMatch(TestData.notCorrectNewMatchDto3()));
    }


    @Test
    void testGetSummaryScoreBoardFiveMatches_Success() {
        //given
        addToScoreBoardAllMatches();
        //when
        List<String> actual = scoreBoardService.getSummary();
        List<String> expected = TestData.summaryForFiveMatches();
        //then
        assertEquals(expected, actual);
    }

    @Test
    void testGetSummaryScoreBoardUpdatedFiveMatches_Success() {
        //given
        addToScoreBoardAllUpdatedMatches();
        //when
        List<String> actual = scoreBoardService.getSummary();
        List<String> expected = TestData.summaryForFiveUpdatedMatches();
        //then
        assertEquals(expected, actual);
    }
}