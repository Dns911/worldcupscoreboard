package com.los.worldcupscoreboard.data.dto;

public record MatchDTO(String homeTeam, String awayTeam) {
}
