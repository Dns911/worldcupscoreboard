package com.los.worldcupscoreboard.data.dto;

public record ScoreDTO(int homeScore, int awayScore) {
}
