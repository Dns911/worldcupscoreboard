package com.los.worldcupscoreboard.exception;

public class MatchNotFoundException extends Exception {
    public MatchNotFoundException(String message) {
        super(message);
    }
}
