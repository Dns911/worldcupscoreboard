package com.los.worldcupscoreboard.exception;

public class InputDataException extends Exception {
    public InputDataException(String message) {
        super(message);
    }
}
