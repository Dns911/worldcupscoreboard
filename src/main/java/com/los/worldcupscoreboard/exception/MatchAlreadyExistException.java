package com.los.worldcupscoreboard.exception;

public class MatchAlreadyExistException extends Exception {
    public MatchAlreadyExistException(String message) {
        super(message);
    }
}
