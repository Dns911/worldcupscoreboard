package com.los.worldcupscoreboard.exception;


public final class ExceptionMessage {

    private ExceptionMessage() {
    }

    public static final String NOT_VALID_SCORE = "NOT valid score";
    public static final String MATCH_NOT_FOUND = "Match NOT found";
    public static final String MATCH_OR_TEAM_ALREADY_EXIST = "Match already started or team already played";
    public static final String NOT_VALID_INPUT_TEAM = "Check input team";
}
