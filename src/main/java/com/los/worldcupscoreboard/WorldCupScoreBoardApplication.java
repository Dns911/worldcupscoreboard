package com.los.worldcupscoreboard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WorldCupScoreBoardApplication {

    public static void main(String[] args) {
        SpringApplication.run(WorldCupScoreBoardApplication.class, args);
    }
}
