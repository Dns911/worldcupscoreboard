package com.los.worldcupscoreboard.service.impl;

import com.los.worldcupscoreboard.data.dto.MatchDTO;
import com.los.worldcupscoreboard.data.dto.ScoreDTO;
import com.los.worldcupscoreboard.entity.Match;
import com.los.worldcupscoreboard.exception.ExceptionMessage;
import com.los.worldcupscoreboard.exception.InputDataException;
import com.los.worldcupscoreboard.exception.MatchAlreadyExistException;
import com.los.worldcupscoreboard.exception.MatchNotFoundException;
import com.los.worldcupscoreboard.service.ScoreBoardService;
import lombok.Getter;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@Getter
public class ScoreBoardServiceImpl implements ScoreBoardService {

    private final List<Match> scoreBoard = new LinkedList<>();

    /**
     * Main method for start new match, after start match add to List scoreBoard
     *
     * @param matchDTO that contains names homeTeam and awayTeam as String.
     * @throws MatchAlreadyExistException In case if match already started
     *                                    or team already played.
     * @throws InputDataException         In case if homeTeam and awayTeam are same
     */
    @Override
    public void startMatch(MatchDTO matchDTO) throws MatchAlreadyExistException, InputDataException {
        checkMatchDTO(matchDTO);
        Optional<Match> matchOptional = findMatchByTeamMatchDTO(matchDTO);
        if (matchOptional.isEmpty()) {
            Match match = Match.builder()
                    .homeTeam(matchDTO.homeTeam())
                    .awayTeam(matchDTO.awayTeam())
                    .build();
            scoreBoard.addLast(match);
        } else {
            throw new MatchAlreadyExistException(ExceptionMessage.MATCH_OR_TEAM_ALREADY_EXIST);
        }
    }

    /**
     * Main method for update match at the scoreBoard
     *
     * @param matchDTO that contains names homeTeam and awayTeam as String.
     * @param scoreDTO that contains score for homeTeam and awayTeam as int.
     * @return Match
     * @throws MatchNotFoundException In case if match isn't exist at the scoreBoard.
     * @throws InputDataException     In case if homeTeam and awayTeam are same,
     *                                or score is not correct for match (ex. we hove 2-3 score and try to update to 1-3)
     */
    @Override
    public Match updateMatch(MatchDTO matchDTO, ScoreDTO scoreDTO) throws InputDataException, MatchNotFoundException {
        checkMatchDTO(matchDTO);
        Optional<Match> matchOptional = findMatchByMatchDTO(matchDTO);
        if (matchOptional.isPresent()) {

            Match match = matchOptional.get();
            updateScore(match, scoreDTO);
            return match;
        } else {
            throw new MatchNotFoundException(ExceptionMessage.MATCH_NOT_FOUND);
        }
    }

    /**
     * Main method for finish match, after finish match remove from List scoreBoard
     *
     * @param matchDTO that contains names homeTeam and awayTeam as String.
     * @throws MatchNotFoundException In case if match isn't exist at the scoreBoard.
     * @throws InputDataException     In case if homeTeam and awayTeam are same
     */
    @Override
    public void finishMatch(MatchDTO matchDTO) throws InputDataException, MatchNotFoundException {
        checkMatchDTO(matchDTO);
        Optional<Match> matchOptional = findMatchByMatchDTO(matchDTO);
        if (matchOptional.isPresent()) {
            scoreBoard.remove(matchOptional.get());
        } else {
            throw new MatchNotFoundException(ExceptionMessage.MATCH_NOT_FOUND);
        }
    }

    /**
     * Main method for get summary information about current matches
     *
     * @return List<String> List of strings, where string is result toString method
     * for matches and return in specific order
     * (first - matches with the most summary score and started the latest).
     */
    @Override
    public List<String> getSummary() {
        return scoreBoard.stream().sorted(Comparator.comparingInt(Match::getSumScore).reversed()
                .thenComparing(scoreBoard::indexOf, Comparator.reverseOrder())).map(Match::toString).toList();
    }

    /**
     * Method for check input data,
     * using in main methods
     *
     * @param matchDTO that contains names homeTeam and awayTeam as String.
     * @throws InputDataException In case if homeTeam and awayTeam are same
     */
    private void checkMatchDTO(MatchDTO matchDTO) throws InputDataException {
        if (matchDTO.homeTeam().equalsIgnoreCase(matchDTO.awayTeam())) {
            throw new InputDataException(ExceptionMessage.NOT_VALID_INPUT_TEAM);
        }
    }

    /**
     * Method for find match at the score board contains 1 or both teams,
     * check if the team already play in some match,
     * using in main methods
     *
     * @param matchDTO that contains names homeTeam and awayTeam as String.
     * @return Optional<Match> and check this result in other methods
     */
    private Optional<Match> findMatchByTeamMatchDTO(MatchDTO matchDTO) {
        return scoreBoard.stream().filter(e ->
                        (e.getHomeTeam().equalsIgnoreCase(matchDTO.homeTeam())
                                || e.getAwayTeam().equalsIgnoreCase(matchDTO.awayTeam()))
                                || (e.getHomeTeam().equalsIgnoreCase(matchDTO.awayTeam())
                                || e.getAwayTeam().equalsIgnoreCase(matchDTO.homeTeam())))
                .findFirst();
    }

    /**
     * Method to find match at the score board contains both teams,
     * using in main methods
     *
     * @param matchDTO that contains names homeTeam and awayTeam as String.
     * @return Optional<Match> and check this result in other methods
     */
    private Optional<Match> findMatchByMatchDTO(MatchDTO matchDTO) {
        return scoreBoard.stream().filter(e ->
                        e.getHomeTeam().equalsIgnoreCase(matchDTO.homeTeam())
                                && e.getAwayTeam().equalsIgnoreCase(matchDTO.awayTeam()))
                .findFirst();
    }

    /**
     * Method to update score for matches at the board,
     * using in main methods
     *
     * @param match    entity Match.
     * @param scoreDTO that contains score for homeTeam and awayTeam as int.
     * @throws InputDataException the score is not correct for match
     *                            (ex. we hove 2-3 score and try to update to 1-3)
     */
    private void updateScore(Match match, ScoreDTO scoreDTO) throws InputDataException {
        if (match.getHomeScore() > scoreDTO.homeScore() || match.getAwayScore() > scoreDTO.awayScore()) {
            throw new InputDataException(ExceptionMessage.NOT_VALID_SCORE);
        }
        match.setHomeScore(scoreDTO.homeScore());
        match.setAwayScore(scoreDTO.awayScore());
    }
}
