package com.los.worldcupscoreboard.service;

import com.los.worldcupscoreboard.data.dto.MatchDTO;
import com.los.worldcupscoreboard.data.dto.ScoreDTO;
import com.los.worldcupscoreboard.entity.Match;
import com.los.worldcupscoreboard.exception.InputDataException;
import com.los.worldcupscoreboard.exception.MatchAlreadyExistException;
import com.los.worldcupscoreboard.exception.MatchNotFoundException;

import java.util.List;

public interface ScoreBoardService {

    List<Match> getScoreBoard();

    void startMatch(MatchDTO matchDTO) throws MatchAlreadyExistException, InputDataException;

    Match updateMatch(MatchDTO matchDTO, ScoreDTO scoreDTO) throws InputDataException, MatchNotFoundException;

    void finishMatch(MatchDTO matchDTO) throws InputDataException, MatchNotFoundException;

    List<String> getSummary();
}
