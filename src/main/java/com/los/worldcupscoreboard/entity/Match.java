package com.los.worldcupscoreboard.entity;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
@EqualsAndHashCode
public class Match {

    private final String homeTeam;
    private final String awayTeam;
    private int homeScore;
    private int awayScore;

    public int getSumScore() {
        return homeScore + awayScore;
    }

    public String toString() {
        return "%s %d - %s %d".formatted(homeTeam, homeScore, awayTeam, awayScore);
    }
}
